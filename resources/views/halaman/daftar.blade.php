<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form.HTML</title>
</head> 
<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/kirim" method="POST">
        @csrf

        <label>First name:</label> <br> <br>
        <input type="text" name="nama_depan"> <br> <br>
        
        <label>Last name:</label><br> <br>
        <input type="text" name="nama_belakang"><br><br>
        
        <label>Gender:</label> <br><br>
        <input type="radio" name="Gender">Male <br>
        <input type="radio" name="Gender">Female <br>
        <input type="radio" name="Gender">Other <br> <br>

        <label>Nationality:</label><br> <br>
        <select name="Negara_Asal" id="">
            <option value="">Indonesia</option>
            <option value="">Malaysia</option>
            <option value="">Singapura</option>
        </select><br><br>

        <label>Language Spoken:</label> <br><br>
        <input type="checkbox" name="Language">Bahasa Indonesia <br>
        <input type="checkbox" name="Language">English <br>
        <input type="checkbox" name="Language">Other <br><br>

        <label>Bio:</label><br><br>
        <textarea name="" id="" cols="30" rows="10"></textarea><br>
        

        <input type="submit" value="Sign Up">
    </Form>

    
</body>
</html>